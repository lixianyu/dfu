
/*
 * Auto generated Run-Time-Environment Component Configuration File
 *      *** Do not modify ! ***
 *
 * Project: 'dfu_dual_bank_ble_s130_pca10028' 
 * Target:  'nrf51422_xxac' 
 */

#ifndef RTE_COMPONENTS_H
#define RTE_COMPONENTS_H

#define BSP_DEFINES_ONLY
#define S130
#define SOFTDEVICE_PRESENT
#define SWI_DISABLE0

#endif /* RTE_COMPONENTS_H */
