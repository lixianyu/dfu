#ifndef CUSTOM_BOARD_H
#define CUSTOM_BOARD_H

#if defined(LEDA_PROJECT)
#define BUTTONS_NUMBER 0
#define BUTTON_START   23
#define BUTTON_1       23
#define BUTTON_STOP    23
#define BUTTON_PULL    NRF_GPIO_PIN_PULLUP

#define BUTTONS_LIST { BUTTON_1 }

#define BSP_BUTTON_0   BUTTON_1

#define BSP_BUTTON_0_MASK (1<<BSP_BUTTON_0)

#define BUTTONS_MASK   BSP_BUTTON_0_MASK
/* LEDs */
#define LEDS_NUMBER    4

#define LED_START      12
#define LED_1          12
#define LED_2          13
#define LED_3          14
#define LED_4          15
#define LED_STOP       15

#define LEDS_LIST { LED_1, LED_2, LED_3, LED_4}

#define BSP_LED_0      LED_4 //green
#define BSP_LED_1      LED_3 //red
#define BSP_LED_2      LED_2 //red
#define BSP_LED_3      LED_1 //green

#define BSP_LED_0_MASK (1<<BSP_LED_0)
#define BSP_LED_1_MASK (1<<BSP_LED_1)
#define BSP_LED_2_MASK (1<<BSP_LED_2)
#define BSP_LED_3_MASK (1<<BSP_LED_3)

#define LEDS_MASK (BSP_LED_0_MASK|BSP_LED_1_MASK|BSP_LED_2_MASK|BSP_LED_3_MASK)
/* all LEDs are lit when GPIO is low */
#define LEDS_INV_MASK (~(LEDS_MASK))
#elif defined(TETHYS_PROJECT)
#define BUTTONS_NUMBER 1
#define BUTTON_START   0
#define BUTTON_1       0
#define BUTTON_STOP    0
#define BUTTON_PULL    NRF_GPIO_PIN_PULLUP

#define BUTTONS_LIST { BUTTON_1 }

#define BSP_BUTTON_0   BUTTON_1

#define BSP_BUTTON_0_MASK (1<<BSP_BUTTON_0)

#define BUTTONS_MASK   0x00000001
///////////////////////////////////////////////////////////////////////////////////////////
#define LEDS_NUMBER    7

#define LED_START      8

#define LED_1          8 //col 4
#define LED_2          9 //col 3
#define LED_3          10//col 2
#define LED_4          11//col 1

#define LED_5          12//rol 3
#define LED_6          13//rol 2
#define LED_7          14//rol 1

#define LED_STOP       14

#define LEDS_LIST { LED_1, LED_2, LED_3, LED_4, LED_5, LED_6, LED_7 }

#define BSP_LED_0      LED_1
#define BSP_LED_1      LED_2
#define BSP_LED_2      LED_3
#define BSP_LED_3      LED_4
#define BSP_LED_4      LED_5
#define BSP_LED_5      LED_6
#define BSP_LED_6      LED_7

#define BSP_LED_COL_4_MASK (1<<BSP_LED_0)
#define BSP_LED_COL_3_MASK (1<<BSP_LED_1)
#define BSP_LED_COL_2_MASK (1<<BSP_LED_2)
#define BSP_LED_COL_1_MASK (1<<BSP_LED_3)

#define BSP_LED_ROL_3_MASK (1<<BSP_LED_4)
#define BSP_LED_ROL_2_MASK (1<<BSP_LED_5)
#define BSP_LED_ROL_1_MASK (1<<BSP_LED_6)

#define LEDS_MASK (BSP_LED_COL_4_MASK|BSP_LED_COL_3_MASK|BSP_LED_COL_2_MASK|BSP_LED_COL_1_MASK|BSP_LED_ROL_3_MASK |BSP_LED_ROL_2_MASK|BSP_LED_ROL_1_MASK)
/* all LEDs are lit when GPIO is low */
#define LEDS_INV_MASK (~(LEDS_MASK))

#define LEDS_COL_MASK (BSP_LED_COL_4_MASK | BSP_LED_COL_3_MASK | BSP_LED_COL_2_MASK | BSP_LED_COL_1_MASK)
#define LEDS_ROL_MASK (BSP_LED_ROL_3_MASK |BSP_LED_ROL_2_MASK|BSP_LED_ROL_1_MASK)

#define TETHYS_LED_ROL_1 LED_7
#define TETHYS_LED_ROL_2 LED_6
#define TETHYS_LED_ROL_3 LED_5
#define TETHYS_LED_COL_1 LED_4
#define TETHYS_LED_COL_2 LED_3
#define TETHYS_LED_COL_3 LED_2
#define TETHYS_LED_COL_4 LED_1
#else
// LEDs definitions for CUSTOM_BOARD Toweer
#define LEDS_NUMBER    1

#ifdef MIMAS_VER3
#define LED_START      17
#define LED_1          17
#define LED_STOP       17
#else
#ifdef MIMAS_VER2
#define LED_START      24
#define LED_1          24
#define LED_STOP       24
#else
#define LED_START      4
#define LED_1          4
#define LED_STOP       4
#endif
#endif

#define LEDS_LIST { LED_1 }

#define BSP_LED_0      LED_1

#define BSP_LED_0_MASK (1<<BSP_LED_0)

#define LEDS_MASK      (BSP_LED_0_MASK)
/* all LEDs are lit when GPIO is low */
#define LEDS_INV_MASK  (~LEDS_MASK)


#define BSP_LED_1  BSP_LED_0
////////////////////////////////////////////////////////////////////////////////////////////////////

#define BUTTONS_NUMBER 1
#ifdef MIMAS_VER3
#define BUTTON_START   24
#define BUTTON_1       24
#define BUTTON_STOP    24
#else
#ifdef MIMAS_VER2
#define BUTTON_START   5
#define BUTTON_1       5
#define BUTTON_STOP    5
#else
#define BUTTON_START   17
#define BUTTON_1       17
#define BUTTON_STOP    17
#endif
#endif
#define BUTTON_PULL    NRF_GPIO_PIN_PULLUP

#define BUTTONS_LIST { BUTTON_1 }

#define BSP_BUTTON_0   BUTTON_1

#define BSP_BUTTON_0_MASK (1<<BSP_BUTTON_0)

#define BUTTONS_MASK   0x00020000
#endif
////////////////////////////////////////////////////////////////////////////////////////////////////

#define SPIM0_SCK_PIN       13     /**< SPI clock GPIO pin number. */
#define SPIM0_MOSI_PIN      12     /**< SPI Master Out Slave In GPIO pin number. */
#define SPIM0_MISO_PIN      11     /**< SPI Master In Slave Out GPIO pin number. */
#define SPIM0_SS_PIN        10     /**< SPI Slave Select GPIO pin number. */

#endif // CUSTOM_BOARD_H
