#ifndef CUSTOM_BOARD_H
#define CUSTOM_BOARD_H

// LEDs definitions for CUSTOM_BOARD Toweer
#define LEDS_NUMBER    1

#define LED_START      4
#define LED_1          4
#define LED_STOP       4

#define LEDS_LIST { LED_1 }

#define BSP_LED_0      LED_1

#define BSP_LED_0_MASK (1<<BSP_LED_0)

#define LEDS_MASK      (BSP_LED_0_MASK)
/* all LEDs are lit when GPIO is low */
#define LEDS_INV_MASK  LEDS_MASK


#define BSP_LED_1  BSP_LED_0
////////////////////////////////////////////////////////////////////////////////////////////////////

#define BUTTONS_NUMBER 1

#define BUTTON_START   17
#define BUTTON_1       17
#define BUTTON_STOP    17
#define BUTTON_PULL    NRF_GPIO_PIN_PULLUP

#define BUTTONS_LIST { BUTTON_1 }

#define BSP_BUTTON_0   BUTTON_1

#define BSP_BUTTON_0_MASK (1<<BSP_BUTTON_0)

#define BUTTONS_MASK   0x00020000

////////////////////////////////////////////////////////////////////////////////////////////////////

#define SPIM0_SCK_PIN       13     /**< SPI clock GPIO pin number. */
#define SPIM0_MOSI_PIN      12     /**< SPI Master Out Slave In GPIO pin number. */
#define SPIM0_MISO_PIN      11     /**< SPI Master In Slave Out GPIO pin number. */
#define SPIM0_SS_PIN        10     /**< SPI Slave Select GPIO pin number. */

#endif // CUSTOM_BOARD_H
